#!/usr/bin/env python3
# Requires pip3 install pms7003

from pms7003 import Pms7003Sensor, PmsSensorException
import json
import os
import paho.mqtt.publish as publish
import argparse

PMS7003_DEVICE = os.getenv('PMS7003_DEVICE', '/dev/ttyUSB0')
MQTT_BROKER_HOST = os.getenv('MQTT_BROKER_HOST', 'fluent-bit')
TOPIC = os.getenv('TOPIC', '/demo/pms7003')

if __name__ == '__main__':

    sensor = Pms7003Sensor(PMS7003_DEVICE)

    while True:
        try:
            try:
                msg = sensor.read()
                publish.single(TOPIC, json.dumps(msg), hostname=MQTT_BROKER_HOST)
                print('Message published to mqtt: {}'.format(msg))
            except Exception as e:
                print('Air quality read successfully but mqtt publish failed with error: {}'.format(e))
        except PmsSensorException:
            print('PMS7003 Connection problem')

    sensor.close()
